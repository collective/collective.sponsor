��          �               $     	   B     L     U     a     o     �  #   �     �     �     �     �     �            �  '  )   �  	   �  	   �                  !   ?  (   a     �     �     �     �     �     �  )   �   A folder which can contain sponsors. Body Text Category Description External link Please enter a Website URL. Please select a category. Short Description of the Body Text. Sponsor Sponsor Content Type Sponsor Folder Title Website URL label_image_field visit_website Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: YEAR-MO-DA HO:MI +ZONE
PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0
Language-Code: en
Language-Name: English
Preferred-Encodings: utf-8 latin1
Domain: DOMAIN
 Ein Ordner, der Sponsoren enthalten kann. Haupttext Kategorie Beschreibung Externer Link Bitte geben Sie eine Website an. Bitte wählen Sie eine Kategorie. Eine kurze Beschreibung des Haupttextes. Sponsor Sponsor Content Typ Sponsoren Ordner Titel Website Adresse Logo oder Bild des Sponsors Besuchen Sie die Website unseres Sponsors 